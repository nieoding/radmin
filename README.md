## RAdmin


```bash
git clone https://gitee.com/nieoding/radmin.git
cd radmin

## 1. 切换antd版本
git checkout -b antd origin/AntDesign
## 2. 切换semiDesign版本
git checkout -b semi origin/SemiDesign
## 3. 切换TDesign版本
git checkout -b td origin/TDesign

npm install
npm run dev

```

国内环境`npm`很容易安装依赖失败，可以改成 `tyarn`，用法参见https://www.npmjs.com/package/tyarn
