import * as service from '@/api/auth'
import { useNavigate } from 'react-router-dom'
export default function(){
  const navigate = useNavigate()
  function handleLogin(){
    service.login({username: 'admin', password: 'admin'}).then(data => {
      // 将token写入本地存储
      window.localStorage.setItem('access-token', data.data.token)
      // 重新进入首页
      navigate('/')
    }).catch(err => {
      alert(err.response.data)
    })
  }
  return <div>login page <a onClick={handleLogin}>login</a></div>
}