import React from "react"

export interface RouteItem {
  name: string,             // 路由唯一标识
  path: string,             // 路由相对路径
  children?: RouteItem[],   // 子路由
  component?: Function,     // 路由元素，类似于element，支持React.lazy
  hidden?: boolean,         // true则不在菜单中显示
  redirect?: string,        // 重定向到哪个路由
  meta?: {
    title: string,          // 菜单显示的名称
    icon?: JSX.Element,     // 菜单显示的图标
    permission?: string[]   // 用户权限
  }
}

export const LOGIN_PATH = '/login'   // 默认登录路径
export const KEY_HOME = 'home'      // 默认主页标识

const routers: RouteItem[] = [
  {
    path: '/',
    name: KEY_HOME,
    component: React.lazy(() => import('@/views/home')),
    meta: {
      title: "首页"
    }
  },
  {
    path: 'demo',
    name: 'demoPage',
    component: React.lazy(() => import('@/views/demo')),
    meta: {
      title: "演示"
    }
  },
]

export default routers;