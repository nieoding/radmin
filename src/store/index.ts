import { RouteItem } from "@/config/router.config";
import { atom } from "jotai";

// 用户信息结构，与后端约定
export interface UserInfo {
  id: number,
  username: string,
  role: string
}

// 本地用户结构
export interface User {
  info?: UserInfo,
  routers?: RouteItem[],  // 根据当前用户角色筛选的路由列表
}

// 会话 - 用户信息
export const userStore = atom<User>({})