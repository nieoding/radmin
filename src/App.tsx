import {BrowserRouter, Route, Routes, Navigate, useLocation} from 'react-router-dom';
import LoginView from '@/views/login'
import NotFoundView from '@/views/404'
import AppLayout from '@/layouts/AppLayout';
import routers from '@/config/router.config';
import { AjaxEffectFragment } from '@/utils/request';
import { renderRouter, RequireAuth } from './utils/router';

function AppRouter(){
  return (
    <BrowserRouter>
      <Routes>
        <Route path="login" element={<LoginView/>}/>
        <Route path="/" element={<RequireAuth><AppLayout/></RequireAuth>}>
          {
            routers.map(item=>renderRouter(item))
          }
        </Route>
        <Route path="*" element={<NotFoundView/>}/>
      </Routes>
    </BrowserRouter>
  )
}

function App() {
  return (
    <>
      <AjaxEffectFragment/>
      <AppRouter/>
    </>
  )
}

export default App
