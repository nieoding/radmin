import { Link, Outlet } from "react-router-dom";
import routers from '@/config/router.config';
import * as service from '@/api/auth'
import { useAtom } from "jotai";
import { userStore } from "@/store";

export default function (){
  const [, setUser] = useAtom(userStore)
  async function handleLogout(){
    if(!confirm("确认退出？")){return}
    await service.logout()
    // 清除本地Token和当前会话
    window.localStorage.removeItem('access-token')
    setUser({})
  }
  return <>
    {/* 头部 */}
    <div>
      {
        routers.map((item, idx)=> <span key={idx}><Link to={item.path}>{item.meta?.title}</Link>&emsp;</span>)
      }
      <a onClick={handleLogout}>logout</a>
    </div>
    {/* 内容体 */}
    <Outlet/>
  </>
}